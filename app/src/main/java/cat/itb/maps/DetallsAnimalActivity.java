package cat.itb.maps;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

public class DetallsAnimalActivity extends AppCompatActivity {

    public String nomAnimal;
    public static StorageReference storageReference;
    public static Uri imgURI;
    public static String imgName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalls_animal);

        TextView detallsnom = findViewById(R.id.detallsnom);
        TextView detallsdescripcio = findViewById(R.id.detallsdesc);
        ImageView detallsfoto = findViewById(R.id.detallsfoto);

        //S'enmagatzema l'intent actual per poder utilitzar el se mètode getExtras() per a obtenir el nom del animal
        Intent intent = getIntent();

        //S'utilitza el mètode getExtras() per a obtenir el nom del animal
        nomAnimal = intent.getExtras().getString("nomAnimal");
        imgName = intent.getExtras().getString("nomImg");

        if (storageReference != null){
            storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Picasso.with(DetallsAnimalActivity.this).load(uri).into(detallsfoto);
                    detallsfoto.setImageURI(uri);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(DetallsAnimalActivity.this, "No s'ha pogut obtenir la imatge", Toast.LENGTH_LONG).show();
                }
            });
        }

        //S'asigna el nom del animal com a valor de text del TextView "detallsnom"
        detallsnom.setText(nomAnimal);

        //S'assigna la descripció de l'animal segons el seu nom
        switch (nomAnimal){
            case "Lleó":
                detallsdescripcio.setText("El lleó (Panthera leo) és una espècie de mamífer carnívor de la família dels fèlids. Es tracta d'un fèlid musculat i de pit profund, amb el cap curt i arrodonit, el coll petit, orelles rodones i un floc de pèls a la punta de la cua.");
                break;
            case "Lleopard":
                detallsdescripcio.setText("El lleopard (Panthera pardus) és un dels cinc grans fèlids del gènere Panthera (els altres són el lleó, el tigre, el jaguar i el lleopard de les neus). Es tracta d'un mamífer carnívor de la família dels felins.");
                break;
            case "Búfal":
                detallsdescripcio.setText("Els búfals són diverses espècies d'animals bòvids dins la família Bovinae i en la tribu Bovini (a la qual també pertanyen entre altres el bou i el iac)");
                break;
            case "Elefant":
                detallsdescripcio.setText("Els elefants són grans mamífers terrestres de l'ordre dels proboscidis.");
                break;
            case "Rinoceront":
                detallsdescripcio.setText("Els rinoceronts (Rhinocerotidae, del grec \"rhino\", nas i \"keras\", banya, nas banyut) són qualsevol de les cinc espècies supervivents de la família dels rinoceròtids.");
                break;
            case "Guepard":
                detallsdescripcio.setText("El guepard (Acinonyx jubatus) és un membre atípic de la família dels fèlids, que caça gràcies a la seva vista i velocitat. És el mamífer terrestre més ràpid: arriba fins a 110 km/h[1] en carreres curtes.");
                break;
            case "Gos salvatge":
                detallsdescripcio.setText("El licaó (Lycaon pictus) és un cànid endèmic del continent africà, que habita normalment a les sabanes.");
                break;
            default:
                detallsdescripcio.setText("No hi han detalls");
                break;
        }

    }
}
