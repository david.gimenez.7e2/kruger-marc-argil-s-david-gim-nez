package cat.itb.maps;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class AddAnimalActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_FOTO_ANIMAL = 112;

    ImageView img;
    StorageReference mStorageRef;
    public Uri imguri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_animal);

        mStorageRef = FirebaseStorage.getInstance().getReference("Imatges");

        EditText editnom = findViewById(R.id.editnomanimal);
        Button guardarbtn = findViewById(R.id.savebtn);

        ImageView lionicon = findViewById(R.id.lionicon);
        ImageView leopardicon = findViewById(R.id.leopardicon);
        ImageView buffaloicon = findViewById(R.id.buffaloicon);
        ImageView elephanticon = findViewById(R.id.elephanticon);
        ImageView rhinoicon = findViewById(R.id.rhino_icon);
        ImageView cheetahicon = findViewById(R.id.cheetahicon);
        ImageView wilddogicon = findViewById(R.id.wilddogicon);
        img = findViewById(R.id.cameraicon);

        lionicon.setOnClickListener(view -> escriureLleo(view, editnom));
        leopardicon.setOnClickListener(view -> escriureLeopard(view, editnom));
        buffaloicon.setOnClickListener(view -> escriureBuffalo(view, editnom));
        elephanticon.setOnClickListener(view -> escriureElephant(view, editnom));
        rhinoicon.setOnClickListener(view -> escriureRhino(view, editnom));
        cheetahicon.setOnClickListener(view -> escriureCheetah(view, editnom));
        wilddogicon.setOnClickListener(view -> escriureWildDog(view, editnom));
        img.setOnClickListener(view -> seleccionarFoto());
        guardarbtn.setOnClickListener(view -> guardarAnimal(editnom));

    }

    private void escriureLleo(View view, EditText editnom) {
        editnom.setText("Lleó");
    }

    private void escriureLeopard(View view, EditText editnom) {
        editnom.setText("Lleopard");
    }

    private void escriureBuffalo(View view, EditText editnom) {
        editnom.setText("Búfal");
    }

    private void escriureElephant(View view, EditText editnom) {
        editnom.setText("Elefant");
    }

    private void escriureRhino(View view, EditText editnom) {
        editnom.setText("Rinoceront");
    }

    private void escriureCheetah(View view, EditText editnom) {
        editnom.setText("Guepard");
    }

    private void escriureWildDog(View view, EditText editnom) {
        editnom.setText("Gos salvatge");
    }

    //Es crea i es crida a un intent per seleccionar una foto de la galeria
    private void seleccionarFoto() {

        Intent selectPictureIntent = new Intent();
        selectPictureIntent.setType("image/*");
        selectPictureIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(selectPictureIntent, REQUEST_CODE_FOTO_ANIMAL);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==REQUEST_CODE_FOTO_ANIMAL && resultCode==RESULT_OK && data!=null && data.getData() != null){

            imguri=data.getData();
            img.setImageURI(imguri);

        }
    }

    private void guardarAnimal(EditText editnom) {

        EditText editnomanimal = findViewById(R.id.editnomanimal);
        String nomanimal = editnomanimal.getText().toString();

        //Tornar el valor del nom del animal al Activity
        Intent intent = new Intent();
        intent.putExtra("nomAnimal", nomanimal);
        setResult(Activity.RESULT_OK, intent);

        if (imguri != null) {
            String imgname = System.currentTimeMillis() + "." + obtenirExtensio(imguri);

            intent.putExtra("nomImg", imgname);

            StorageReference Ref = mStorageRef.child(imgname);

            KrugerActivity.storageReference = Ref;
            KrugerActivity.imgURI = imguri;

            Ref.putFile(imguri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            // Get a URL to the uploaded content
                            Toast.makeText(AddAnimalActivity.this, "S'ha pujat la foto correctament", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            // Handle unsuccessful uploads
                            Toast.makeText(AddAnimalActivity.this, "No s'ha pogut pujar la foto", Toast.LENGTH_SHORT).show();
                        }
                    });
        }
        else
        {
            KrugerActivity.imgURI = null;
            KrugerActivity.storageReference = null;
        }
        finish();
    }

    //Mètode que serveix per obtenir el valor del nom del animal que s'ha assignat al intent
    public static String obtenirAnimal(Intent intent){
        return intent.getStringExtra("nomAnimal");
    }

    //Mètode que serveix per obtenir el valor del nom de la imatge que s'ha assignat al intent
    public static String obtenirImgName(Intent intent){
        return intent.getStringExtra("nomImg");
    }

    //Mètode que es fa servir per obtenir l'extensió d'una imatge
    public String obtenirExtensio(Uri uri){

        ContentResolver cr = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(cr.getType(uri));

    }

}
