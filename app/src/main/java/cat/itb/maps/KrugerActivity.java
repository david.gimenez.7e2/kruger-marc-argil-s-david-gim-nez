package cat.itb.maps;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.location.ActivityTransition;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Map;

public class KrugerActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {

    private static final int REQUEST_LOCATION = 1;
    public static final int REQUEST_CODE_NOM_ANIMAL = 111;
    public GoogleMap mMap;
    private LocationManager locationManager;

    public ArrayList<ArrayList>markers = new ArrayList<>();

    public String nomAnimal = "ERROR";

    public String imgName;
    public static StorageReference storageReference;
    public static Uri imgURI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityCompat.requestPermissions(this,new String[]
            {Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

        setContentView(R.layout.initial_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */

    @Override
    public void onMapReady(GoogleMap googleMap) {

        Button addbutton = findViewById(R.id.addbtn);

        mMap = googleMap;

        //mMap.setMinZoomPreference(9);               //Maxim lluny
        mMap.setMaxZoomPreference(16);              //Maxim Zoom

        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        //S'estableix l'ubicació inicial de la càmera del mapa al "Kruger National Park"
        LatLng Default = new LatLng(-23.987399, 31.556050);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(Default));

        addbutton.setOnClickListener(view -> addMarker(view, mMap));

    }

    private void addMarker(View view, GoogleMap map) {

        //double [] lastLocation = new double[]{};
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        //Mira si el GPS està activat
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {

            //Es crida al mètode per activar el GPS
            activarGPS();

        }

        //Es crida al mètode per afegir un nou animal
        anarAAddAnimalActivity();

    }

    //Crea un intent i inicia l'Activity AddAnimalActivity
    private void anarAAddAnimalActivity() {

        Intent intent = new Intent(KrugerActivity.this, AddAnimalActivity.class);
        startActivityForResult(intent, REQUEST_CODE_NOM_ANIMAL);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        GoogleMap map = mMap;

        double [] lastLocation = new double[]{};
        lastLocation = obtenirUbicacio();

        String iconBase = "https://maps.google.com/mapfiles/kml/shapes/";

        switch (requestCode) {
            case REQUEST_CODE_NOM_ANIMAL:
                if (resultCode == Activity.RESULT_OK) {
                    String nomAnimal = AddAnimalActivity.obtenirAnimal(data);
                    String imgName = AddAnimalActivity.obtenirImgName(data);
                    Toast.makeText(this, "S'ha afegit un nou "+nomAnimal.toLowerCase(), Toast.LENGTH_SHORT).show();
                    this.nomAnimal = nomAnimal;
                    this.imgName = imgName;
                } else {
                    Toast.makeText(this, "Tornant al mapa...", Toast.LENGTH_SHORT).show();
                    this.nomAnimal = "ERROR";
                }
        }

        if (!nomAnimal.equals("ERROR") && (!nomAnimal.equals("")) && (nomAnimal != null)){
            if (lastLocation != null)
            {
                double longitude = lastLocation[0];
                double latitude = lastLocation[1];

                LatLng newLocation = new LatLng(longitude, latitude);

                ArrayList<Object> detallsMarker = new ArrayList<>();

                MarkerOptions marker = new MarkerOptions();

                switch (nomAnimal){
                    case "Lleó":
                        marker = new MarkerOptions().position(newLocation).title(nomAnimal).icon(bitmapDescriptorFromVector(getApplicationContext(),R.drawable.lion_icon_marker));
                        map.addMarker(marker);
                        detallsMarker.add(marker);
                        detallsMarker.add(storageReference);
                        detallsMarker.add(imgName);
                        break;
                    case "Lleopard":
                        marker = new MarkerOptions().position(newLocation).title(nomAnimal).icon(bitmapDescriptorFromVector(getApplicationContext(),R.drawable.leopard_icon_marker));
                        map.addMarker(marker);
                        detallsMarker.add(marker);
                        detallsMarker.add(storageReference);
                        detallsMarker.add(imgName);
                        break;
                    case "Búfal":
                        marker = new MarkerOptions().position(newLocation).title(nomAnimal).icon(bitmapDescriptorFromVector(getApplicationContext(),R.drawable.buffalo_icon_marker));
                        map.addMarker(marker);
                        detallsMarker.add(marker);
                        detallsMarker.add(storageReference);
                        detallsMarker.add(imgName);
                        break;
                    case "Elefant":
                        marker = new MarkerOptions().position(newLocation).title(nomAnimal).icon(bitmapDescriptorFromVector(getApplicationContext(),R.drawable.elephant_icon_marker));
                        map.addMarker(marker);
                        detallsMarker.add(marker);
                        detallsMarker.add(storageReference);
                        detallsMarker.add(imgName);
                        break;
                    case "Rinoceront":
                        marker = new MarkerOptions().position(newLocation).title(nomAnimal).icon(bitmapDescriptorFromVector(getApplicationContext(),R.drawable.rhino_icon_marker));
                        map.addMarker(marker);
                        detallsMarker.add(marker);
                        detallsMarker.add(storageReference);
                        detallsMarker.add(imgName);
                        break;
                    case "Guepard":
                        marker = new MarkerOptions().position(newLocation).title(nomAnimal).icon(bitmapDescriptorFromVector(getApplicationContext(),R.drawable.cheetah_icon_marker));
                        map.addMarker(marker);
                        detallsMarker.add(marker);
                        detallsMarker.add(storageReference);
                        detallsMarker.add(imgName);
                        break;
                    case "Gos salvatge":
                        marker = new MarkerOptions().position(newLocation).title(nomAnimal).icon(bitmapDescriptorFromVector(getApplicationContext(),R.drawable.wild_dog_icon_marker));
                        map.addMarker(marker);
                        detallsMarker.add(marker);
                        detallsMarker.add(storageReference);
                        detallsMarker.add(imgName);
                        break;
                    default:
                        marker = new MarkerOptions().position(newLocation).title(nomAnimal).icon(bitmapDescriptorFromVector(getApplicationContext(),R.drawable.interrogant_icon_marker));
                        map.addMarker(marker);
                        detallsMarker.add(marker);
                        detallsMarker.add(storageReference);
                        detallsMarker.add(imgName);
                        break;
                }

                markers.add(detallsMarker);

                mMap.setOnInfoWindowClickListener(this::onInfoWindowClick);

                mMap.moveCamera(CameraUpdateFactory.newLatLng(newLocation));

                DetallsAnimalActivity.storageReference = storageReference;

            }
        }

        this.mMap = map;

    }

    private double [] obtenirUbicacio() {

        //Es torna a mirar els permisos
        if (ActivityCompat.checkSelfPermission(KrugerActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(KrugerActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this,new String[]
                    {Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        }
        else{
            Location ubicacioGps = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location ubicacioInternet = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            Location ubicacioPassiva = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

            if (ubicacioGps != null)
            {
                double lat = ubicacioGps.getLatitude();
                double lng = ubicacioGps.getLongitude();

                return new double[] {lat, lng};

            }
            else if (ubicacioInternet != null)
            {
                double lat = ubicacioInternet.getLatitude();
                double lng = ubicacioInternet.getLongitude();

                return new double[] {lat, lng};
            }
            else if (ubicacioPassiva != null)
            {
                double lat = ubicacioPassiva.getLatitude();
                double lng = ubicacioPassiva.getLongitude();

                return new double[] {lat, lng};
            }
            else
            {
                Toast.makeText(this, "No s'ha pogut obtenir la teva ubicació", Toast.LENGTH_SHORT).show();
                return null;
            }
        }

        return null;
    }

    private void activarGPS() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage("Activar GPS").setCancelable(false).setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));

            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();

            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth()/5, vectorDrawable.getIntrinsicHeight()/5);
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth()/5, vectorDrawable.getIntrinsicHeight()/5, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Intent intent = new Intent(KrugerActivity.this, DetallsAnimalActivity.class);
        intent.putExtra("nomAnimal", marker.getTitle());

        for (int i=0; i<markers.size(); i++){
            ArrayList <Object> aux = markers.get(i);
            if (marker == aux.get(0)){
                intent.putExtra("nomImg", aux.get(2).toString());
                DetallsAnimalActivity.storageReference = (StorageReference) aux.get(1);
            }
        }

        startActivity(intent);
    }
}
