# Kruger National Park

>  Desenvolupat per David Gimenez Rodriguez i Marc Argilés Coloma

### Descripció

Una aplicació realitzada per tal de poder consultar i afegir avistaments d'animala al parc natural "Kruger".

### Desenvolupament

L'aplicació consta de 3 Activities, cada una amb el seu respectiu layout.

**_1. KrugerActivity -->_** Activity on es troben tots els metodes amb relació amb mostrar i afegir animal, i trobar la localització d'aquests.

* [KrugerActivityActivity](https://imgur.com/a/zRSyyAm)

**_2. AddAnimalActivity -->_** Activity on es realitza el procés d'afegir un animal, com ara escriure el seu nom (o clicar la seva icona per tal que l'escrigui automàticament), el afegir una image de la galeria i guardar les dades, això últim implica retornar la informació i pujar-la a la firebase.

* [AddAnimalActivity](https://imgur.com/a/VM2Sapz)

**_3. DetallsAnimalActivity -->_** Activity on mostra l'informació especifica de l'avistament triat, aquesta mostra el nom, l'imatge i una breu descripció d'aquest.

* [DetallsAnimalActivity](https://imgur.com/a/lMBsEcQ)

S'han utilitzat moltes icones, per representar cada animal i també per substituir les marques seves bàsiques al mapa.

**_Millores realitzades -->_** A l'apartat multimèdia puja i descargar d'imatges a partir del fire base i a l'apartat del mapa, cada animal surt representat per la seva imatge/icone.

**_Les especificacions utilitzades han sigut -->_** Android Studio 3.5.3, Google Maps Api i Fire Base.
